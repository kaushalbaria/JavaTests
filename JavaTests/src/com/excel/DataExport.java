package com.excel;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import jxl.Workbook;
import jxl.write.DateTime;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class DataExport {
	
	public static void main(String aa[]) throws Exception {
		exportData();
	}

	public static void exportData() throws Exception {
		{
			try {
				File exlFile = new File("test.xls");
				WritableWorkbook writableWorkbook = Workbook
						.createWorkbook(exlFile);

				WritableSheet writableSheet = writableWorkbook.createSheet(
						"Sheet1", 0);

				// Create Cells with contents of different data types.
				// Also specify the Cell coordinates in the constructor
				for(int i=0;i<5;i++)
				{
					
						Label label = new Label(0, i, " test "+" "+i+" 0");
						Label label1 = new Label(1, i, " test "+" "+i+" 1");
						Label label2 = new Label(2, i, " test "+" "+i+" 2");
						//Label label1 = new Label(i, j, " test "+" "+i+" "+j);
						//DateTime date = new DateTime(i, j, new Date());

						// Add the created Cells to the sheet
						writableSheet.addCell(label);
						writableSheet.addCell(label1);
						writableSheet.addCell(label2);
						//writableSheet.addCell(label1);
						//writableSheet.addCell(date);
					
				}
				
				// Write and close the workbook
				writableWorkbook.write();
				writableWorkbook.close();

			} catch (IOException e) {
				e.printStackTrace();
			} catch (RowsExceededException e) {
				e.printStackTrace();
			} catch (WriteException e) {
				e.printStackTrace();
			}
		}
	}


}
