package com.excel;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ParseExcel {

        private String inputFile;
        static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
        static final String DB_URL = "jdbc:mysql://localhost/mbamigdb";
        static final String USER = "root";
        static final String PASS = "pass";
       


        public void setInputFile(String inputFile) {
                this.inputFile = inputFile;
        }

        public void readNCAClltrl() throws IOException, SQLException, ClassNotFoundException, ParseException  {
                File inputWorkbook = new File(inputFile);
                ArrayList[] NCA = new ArrayList[32];
                String[][] arrayTemp = new String[301][32];
                Workbook w;
                
                
                try {
                        w = Workbook.getWorkbook(inputWorkbook);
                        // Get the first sheet
                        Sheet sheet = w.getSheet(0);
                        // Loop over first 10 column and lines

                        for (int i = 0; i < sheet.getRows(); i++) {
                        	for (int j = 0; j <sheet.getColumns(); j++) {
                                        Cell cell = sheet.getCell(j, i);
                                        arrayTemp[i][j] = cell.getContents();
//                                                System.out.println(cell.getContents());
                                }
                        }
                        int k=0;
                        
                        Class.forName("com.mysql.jdbc.Driver");

                        Connection conn = null;
                        Statement stmt = null;
                        
                        conn = DriverManager.getConnection(DB_URL, USER, PASS);
                        
                        stmt = conn.createStatement();
                        
                        
                       // String sql = "INSERT INTO `mbadb`.`nca_test` (`areaName`, `dateTargt`, `ncaTargt`) VALUES ('"+"aa"+"', '"+new java.sql.Timestamp(new Date().getTime())+"', '"+"11"+"');";
                		
                        for (int i = 1; i <301; i++) {
//                        	System.out.println(arrayTemp[i][0]);
                        	for (int j = 1; j < 31; j++) {
                        		    System.out.println(arrayTemp[i][0]+".."+arrayTemp[i][j]);
                        		   
                                  String sql = "INSERT INTO `mbadb`.`nca_test` (`areaName`, `dateTargt`, `ncaTargt`) VALUES ('"+arrayTemp[i][0]+"', '"+"4/"+j+"/2017 12:00:00 AM"+"', '"+arrayTemp[i][j]+"');";
                                  stmt.executeUpdate(sql); 
                                  //System.out.println(cell.getContents());
                                }
                        }
                        
                } catch (BiffException e) {
                        e.printStackTrace();
                }
        }
        
        public void readNCA() throws IOException, SQLException, ClassNotFoundException, ParseException  {
            File inputWorkbook = new File(inputFile);
            ArrayList[] NCA = new ArrayList[32];
            String[][] arrayTemp = new String[301][32];
            Workbook w;
            
            Class.forName("com.mysql.jdbc.Driver");

            Connection conn = null;
            Statement stmt = null;
            
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            
            stmt = conn.createStatement();
            
            try {
                    w = Workbook.getWorkbook(inputWorkbook);
                    // Get the first sheet
                    Sheet sheet = w.getSheet(0);
                    // Loop over first 10 column and lines

                    for (int i = 0; i < sheet.getRows(); i++) {
                    	for (int j = 0; j <sheet.getColumns(); j++) {
                                    Cell cell = sheet.getCell(j, i);
                                    arrayTemp[i][j] = cell.getContents();
//                                            System.out.println(cell.getContents());
                            }
                    }
                    int k=0;
                                        
                   // String sql = "INSERT INTO `mbadb`.`nca_test` (`areaName`, `dateTargt`, `ncaTargt`) VALUES ('"+"aa"+"', '"+new java.sql.Timestamp(new Date().getTime())+"', '"+"11"+"');";
            		
                    for (int i = 1; i <301; i++) {
//                    	System.out.println(arrayTemp[i][0]);
                    	for (int j = 1; j < 31; j++) {
                    		    System.out.println(arrayTemp[i][0]+".."+arrayTemp[i][j]);
                    		   
                              String sql = "update `mbadb`.`nca_test`  set `ncaTargt`= '"+arrayTemp[i][j]+"' where `areaName`='"+arrayTemp[i][0]+"' and `dateTargt`="+"'4/"+j+"/2017 12:00:00 AM'"+";";
                              System.out.println(sql);
                              stmt.executeUpdate(sql); 
                              //System.out.println(cell.getContents());
                            }
                    }
                    
            } catch (BiffException e) {
                    e.printStackTrace();
            }
    }
	
        public void readNCAData() throws IOException, SQLException, ClassNotFoundException, ParseException  {
            File inputWorkbook = new File(inputFile);
            ArrayList[] NCA = new ArrayList[32];
            String[][] arrayTemp = new String[3291][32];
            Workbook w;
            
            
            try {
                    w = Workbook.getWorkbook(inputWorkbook);
                    // Get the first sheet
                    Sheet sheet = w.getSheet(0);
                    // Loop over first 10 column and lines
                    Class.forName("com.mysql.jdbc.Driver");

                    Connection conn = null;
                    Statement stmt = null;
                    
                    conn = DriverManager.getConnection(DB_URL, USER, PASS);
                    
                    stmt = conn.createStatement();
                    

                    for (int i = 0; i < sheet.getRows(); i++) {
                    	for (int j = 0; j <sheet.getColumns(); j++) {
                                    Cell cell = sheet.getCell(j, i);
                                    arrayTemp[i][j] = cell.getContents();
//                                            System.out.println(cell.getContents());
                            }
                    }
                    int k=0;
                    int count = 1;
                    for (int i = 1; i <sheet.getRows(); i++) {
                   	for (int j = 2; j < sheet.getColumns(); j++) {
                   		
                   		String sqlUpdate = "update `mbamigdb`.`mig_nca_targt_achvmnt` set `dateTargt`='"+arrayTemp[0][j]+" 12:00:00 AM' where `branch_name`='"+arrayTemp[i][1]+"'";
                   		//String sqlUpdate = "update `mbamigdb`.`mig_nca_targt_achvmnt` set `clltrl_achvmnt`='"+arrayTemp[i][j]+"'where `branch_name`='"+arrayTemp[i][1]+"' and `dateTargt`='"+arrayTemp[0][j]+"'";
                   		//String sqlUpdate = "update `mbamigdb`.`mig_nca_targt_achvmnt` set `clltrl_targt`='"+arrayTemp[i][j]+"'where `branch_name`='"+arrayTemp[i][1]+"' and `dateTargt`='"+arrayTemp[0][j]+"'";
                   		//String sqlUpdate = "update `mbamigdb`.`mig_nca_targt_achvmnt` set `nca_targt`='"+arrayTemp[i][j]+"'where `branch_name`='"+arrayTemp[i][1]+"' and `dateTargt`='"+arrayTemp[0][j]+"'";
                      System.out.println(sqlUpdate);
                   		//String sql = "INSERT INTO `mbamigdb`.`mig_nca_targt_achvmnt` (`branch_name`,`branch_cd`, `dateTargt`, `nca_targt`) VALUES ('"+arrayTemp[i][1]+"','"+arrayTemp[i][0]+"', '"+arrayTemp[0][j]+"', '"+arrayTemp[i][j]+"');";
                      //stmt.executeUpdate(sql); 
                   		stmt.executeUpdate(sqlUpdate); 
                   			System.out.println(i+"..."+(j-1)+"..."+arrayTemp[i][0]+".."+arrayTemp[i][1]+".."+arrayTemp[0][j]+".."+arrayTemp[i][j]);
                    		    count++;
                            }
                    }
                    System.out.println("Count... "+count);
                    System.out.println("row... "+sheet.getRows());
                    System.out.println("Column... "+sheet.getColumns());
                    System.out.println("Column... "+sheet.getRows()*sheet.getColumns());
                    System.out.println("total..."+(3290*22));
            } catch (BiffException e) {
                    e.printStackTrace();
            }
    }
        
	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException, ParseException {
		ParseExcel test = new ParseExcel();
		//test.setInputFile("C:/Users/Map My Marketing/Desktop/NCAClltrl.xls");
		//test.setInputFile("E:/DataMyFldr/NCA/NEW With Branch/NCADataWithBranchAchvmnt.xls");
		//test.setInputFile("E:/DataMyFldr/NCA/NEW With Branch/NCADataWithBranchTarget.xls");
		//test.setInputFile("E:/DataMyFldr/NCA/NEW With Branch/NCAClltrlDataWithBranchTarget.xls");
		//test.setInputFile("E:/DataMyFldr/NCA/NEW With Branch/NCAClltrlDataWithBranchAchvmnt.xls");
		test.setInputFile("E:/DataMyFldr/NCA/NEW With Branch/012604/template.xls");
		//test.readNCA();
		test.readNCAData();
	}

}
