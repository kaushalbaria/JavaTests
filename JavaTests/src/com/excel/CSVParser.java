package com.excel;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CSVParser {

	public static void main(String[] args) {

		String csvFile = "C:\\Users\\Map My Marketing\\Desktop\\myFldr\\dataprocessingforzone5\\ho_dtls_20170331.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		try {

			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] country = line.split(cvsSplitBy);

				System.out.println("Emp_Code "+ country[0] + " | Emp_Name " + country[1] + " | Post_Name "+ country[2] + " | Designation "+country[3]+ " | Mob_No "+country[4]+ " | Status "+country[5]);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
