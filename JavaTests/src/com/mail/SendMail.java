package com.mail;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.excel.DataExport;

public class SendMail {

	public void sendMail() throws Exception
	{
		String email = "kaushal.baria@mapmymarketing.ai";
		String subject = "Testing...Sorry..!!:) ";
		//String msg = request.getParameter("message");
		//String htmlmsg = "<h1><center>"+msg+"</center></h1>";
		String htmlmsg = "<center><h1 style=\"text-align: center;color: #195A94;font-size: 350%;\">Map My Marketing</h1>"
				+ "<p style=\"text-align: center;font-family: Consolas;font-size:130%;\">"
				+ "Test Pvt. Ltd."
				+ "<br><br> </p>"
				+ "</center>"
				+ "<h2><b>Testing...</b>";		// code for sending main

		final String username = "imkushbaria@gmail.com";
		final String password = "kush1234?!";
		
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username,password);
				}
			});

		try {

			Message message = new MimeMessage(session);
			try {
				message.setFrom(new InternetAddress(username,"Map My Marketing"));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(email));
			message.setSubject(subject);
			//message.setText(msg);
			message.setContent(htmlmsg, "text/html");
			
			DataExport.exportData();
			
			BodyPart messageBodyPart = new MimeBodyPart();

	         // Now set the actual message
	         messageBodyPart.setText("Hello... This is testing.... Sorry..!!! :)");

	         // Create a multipar message
	         Multipart multipart = new MimeMultipart();

	         // Set text message part
	         multipart.addBodyPart(messageBodyPart);

	         // Part two is attachment
	         messageBodyPart = new MimeBodyPart();
	         String filename = "write_test.xls";
	         DataSource source = new FileDataSource(filename);
	         messageBodyPart.setDataHandler(new DataHandler(source));
	         messageBodyPart.setFileName(filename);
	         multipart.addBodyPart(messageBodyPart);

	         // Send the complete message parts
	         message.setContent(multipart);
			
			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		Timer timer = new Timer();
		final Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR_OF_DAY, 19);
		today.set(Calendar.MINUTE, 25);
		today.set(Calendar.SECOND, 0);
		
		TimerTask tt = new TimerTask(){
			public void run(){
				//Calendar cal = Calendar.getInstance(); //this is the method you should use, not the Date(), because it is desperated.
 
				int hour = today.get(Calendar.HOUR_OF_DAY);//get the hour number of the day, from 0 to 23
				int minute = today.get(Calendar.MINUTE);
				int second = today.get(Calendar.SECOND);
				System.out.println("Current Time.... "+hour+"  "+minute+" "+second);
				SendMail send = new SendMail();
				System.out.println(today.getTime());
				try {
					send.sendMail();
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("doing the scheduled task");
			}
		};												//TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)
		timer.schedule(tt,  today.getTime(), 30*1000);//	delay the task 1 second, and then run task every five seconds
	}

}
