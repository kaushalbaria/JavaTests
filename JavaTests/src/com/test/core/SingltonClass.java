package com.test.core;

public class SingltonClass {

	private static SingltonClass instance;

	private SingltonClass(){}
	
	public static SingltonClass getInstance()
	{
		if(instance==null)
			instance = new SingltonClass();
		
		return instance;
	}
	
	public static void main(String[] args) {
		System.out.println(SingltonClass.getInstance().hashCode());
		System.out.println(SingltonClass.getInstance().hashCode());
		System.out.println(SingltonClass.getInstance().hashCode());
	}
}


