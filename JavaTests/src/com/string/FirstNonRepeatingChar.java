package com.string;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FirstNonRepeatingChar {

	public static void main(String[] args) {
		
		firstNonRepetingChar("Java2Novice");

	}
	
	public static char firstNonRepetingChar(String str)
	{
		Map<Character, Integer> mapTemp = new HashMap<Character, Integer>();
		
		char[] charaArray = str.toCharArray();
		
		int i=1;
		
		for(Character charTemp : charaArray)
		{
			mapTemp.put(charTemp, mapTemp.containsKey(charTemp)?mapTemp.get(charTemp)+1:1);
/*			if(mapTemp.containsKey(charTemp))
			{
				mapTemp.put(charTemp, mapTemp.get(charTemp)+1);
			}
			else
			{
				mapTemp.put(charTemp, 1);
			}*/
		}
		
		Set<Character> charTemp1 = mapTemp.keySet();
		for(Character chr : charTemp1 )
		{
			System.out.println(chr+"..."+mapTemp.get(chr));
		}
		
		return 0;
		
	}

}
